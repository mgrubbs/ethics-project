This repository holds the pseudocode and toy data set for our project. Our code
is meant to generate weights based on the data's demographics that can be used
in other algorithms.

pseudocode.txt has a psuedocode for the algorithm that we would use to generate
and store weights based on the data set. It also contains some comments on what
it would do.

data.in contains a toy data set with no data except for demographic data that 
was collected from a company/group. This demographic data could be interchanged 
to fit whatever groups the company/group wants to diversify in their code.

data.out shows a toy output. The expectation would be that these weights would 
be used in another algorithm. For example, if Twitter was looking to diversify
their trending topics, then they could use these weights in addition to other 
pieces of data to trend a topic.

The immediate way that these weights would be useful would be that they would 
allow a social media to weight more minority groups in trying to trend a creator
or influencer. However, this can also be seen the other way around. For example, 
if police reports are showing that the majority of suspects for a case are black 
in a city, but no one actually knows who the culprit is, the larger number of 
black people being represented would give a smaller weight, lowering their 
discrimination by the data given. 